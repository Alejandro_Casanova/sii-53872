#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include "comun.h"

int main(int argc, char **argv) {
	
	if (argc!=1)  {
		fprintf (stderr, "%s no recibe parametros.\n", argv[0]);
		return(1);
	}
	
	int fd;

	// Borra FIFO por si existía previamente
	unlink("FIFO");
	
	// Crea el FIFO
	if (mkfifo("FIFO", 0600) < 0) {
		perror("No puede crearse el FIFO");
		return(1);
	}

	// Abre el FIFO
	if ((fd=open("FIFO", O_RDONLY)) < 0) {
		perror("No puede abrirse el FIFO");
		return(1);
	}

	mensaje m;
		
	while (read(fd, &m, sizeof(m))==sizeof(m)) {		
		if (m.puntos < 0) {
			fprintf(stderr, "Puntuacion no valida, debe ser mayor o igual que 0\n");
			continue;
		}
		//Imprimo Datos
		fprintf(stdout, "%s marca 1 punto, lleva un total de %d puntos.\n", m.nombre, m.puntos); 
	}

	close(fd);
	unlink("FIFO");
	
	return(0);
}
