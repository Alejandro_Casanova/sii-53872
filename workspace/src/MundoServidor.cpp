// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include "DatosMemCompartida.h"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <math.h>
#include "comun.h"
#include <iostream>
#include <cassert>
#include <signal.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void captura(int s) { //Captura de las señales
	printf("\nSeñal capturada: %s\n", strsignal(s));
	
	if(s == SIGUSR2){
		exit(0);
	}else{
		exit(s);
	}
}

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	acabar = 1;
	
	socket_conexion.Close();
	//socket_comunicacion.Close();
	for(auto &i : conexiones)
		i.Close();
		
	close(fd_fifo);
		
	pthread_join(thid1, NULL);
	pthread_join(thid2, NULL);
	pthread_join(thid3, NULL);
	
	
	
	/*
	//Cierra los FIFO
	close(fd_fifo_servidor_a_cliente);
	unlink(id_fifo_servidor_a_cliente);
	
	close(fd_fifo_cliente_a_servidor);
	unlink(id_fifo_cliente_a_servidor);
	*/
	
		
	
	
	
}

void CMundo::nuevaPartida(){
	puntos1 = 0;
	puntos2 = 0;
	esfera.centro.x = 0.0f;
	esfera.centro.y = 0.0f;
	esfera.radio = 0.5f;
	printf("Nueva Partida!\n");
	jugador1.y1 = -1;
	jugador1.y2 = 1;
	jugador2.y1 = -1;
	jugador2.y2 = 1;
	jugador1.velocidad.y = 0;
	jugador2.velocidad.y = 0;
}

void CMundo::actualizaNombres(){
	
	if(!conexiones.empty()){
		memcpy(nombreJugador1, conexiones[0].getNombre(), strlen(conexiones[0].getNombre()) + 1);
		if(conexiones.size() > 1){
			memcpy(nombreJugador2, conexiones[1].getNombre(), strlen(conexiones[1].getNombre()) + 1);
		}
	}
	
}


void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1 (%s): %d", nombreJugador1, puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2 (%s): %d", nombreJugador2, puntos2);
	print(cad,600,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	static mensaje m; //No es necesario inicializarlo continuamente
	
	for(i=0;i<paredes.size();i++) //Rebote de la pelota y jugadores con las paredes
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	//Rebote de los jugadores con la pelota
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	
	if(fondo_izq.Rebota(esfera)) //Punto del jugador 2
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		m = (mensaje){puntos2, "Jugador 2\0"};
		write(fd_fifo, &m, sizeof(m));

	}

	if(fondo_dcho.Rebota(esfera)) //Punto del jugador 1
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		m = (mensaje){puntos1, "Jugador 1\0"};
		write(fd_fifo, &m, sizeof(m));
	
	}
	
	// ENVÍO SERVIDOR -> CLIENTES (SOCKET)
	//Crear la cadena
	char cad[200];
	sprintf(cad, "%f %f %f %f %f %f %f %f %f %f %f %d %d", 
		esfera.centro.x, esfera.centro.y, esfera.radio, 
		jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, 
		jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, 
		puntos1, puntos2);
	
	/*
	//Envía por el socket
	int msgLen = socket_comunicacion.Send(cad, sizeof(cad));
	if (msgLen < 0)
		perror("Error Socket Send Servidor: ");
	*/
		
	//Envía por los socket
	for(int i = conexiones.size() - 1; i >= 0; i--){
		int msgLen = conexiones[i].Send(cad, sizeof(cad));
		if (msgLen < 0){
			perror("Error Socket Send Servidor. Posible Desconexión del Cliente");
			printf("SE DESCONECTA: JUGADOR %d\n", i + 1);
			if(i == 0){ //Se desconecta el Jugador 1
				//printf("Se desconecta jugador 1\n");
				if(conexiones.size() > 2){ //Se sustituye por un jugador de la cola
					conexiones[0] = conexiones[2];
					conexiones.erase(conexiones.begin() + 2);
				}
				else{ //Si no hay jugadores en la cola
					conexiones[0].Close();
					printf("Esperando Nuevo Jugador...\n");
					while(conexiones[0].getSock() == INVALID_SOCKET); //Bloquea hasta entrada de nuevo jugador
				}
			}else
				conexiones.erase(conexiones.begin()+i);
			
			if(conexiones.size() < 2){
				printf("Esperando Nuevo Jugador...\n");
				while(conexiones.size() < 2); //Bloquea hasta entrada de nuevo jugador
			}
			
			if (i < 2){ // Ha salido un jugador (no espectador)
				nuevaPartida();
			}
		actualizaNombres();
		}
	}
	
	/*
	for (i=conexiones.size()-1; i>=0; i--) {
		if (conexiones[i].Send(cad,200) <= 0) {
			conexiones.erase(conexiones.begin()+i);
		if (i < 2) // Hay menos de dos clientes conectados
		// Se resetean los puntos a cero
		}
     	}
     	*/
     	
	//VICTORIA //////////////////////////////
	if(puntos1 >= VICTORY_SCORE){
		printf("Jugador 1 Gana.\n");
		nuevaPartida();
		
		//acabar = 1;
		//exit(0);
	}else if(puntos2 >= VICTORY_SCORE){
		printf("Jugador 2 Gana.\n");
		nuevaPartida();
		
		//acabar = 1;
		//exit(0);
	}
	
	//Actualiza los datos de memoria compartida
	//pDatosMemCom->esfera = esfera;
	//pDatosMemCom->raqueta1 = jugador1;
	
	//Mueve al jugador controlado por la IA
	//if(pDatosMemCom->accion == 1)
	//	OnKeyboardDown('w', 0, 0);
	//else if (pDatosMemCom->accion == -1)
	//	OnKeyboardDown('s', 0, 0);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
	*/
}

void CMundo::RecibeComandosJugador()
{
	//Para saber qué Jugador se está controlando
	static int nJugador = -1;
	nJugador++;
	int whoAmI = nJugador;
	
	//while ( puntos1 < VICTORY_SCORE && puntos2 < VICTORY_SCORE ) {
	while ( !acabar ) {
		usleep(10);

		unsigned char key;
		if(conexiones.size() > whoAmI){
			if(conexiones[whoAmI].getSock() == INVALID_SOCKET)
				continue;
			if( conexiones[whoAmI].Receive((char*)&key, sizeof(key)) < 0){
				fprintf(stderr, "Error recibeComandosJugador. Jugador %d\n", whoAmI + 1);
				//break;
			}

			if(whoAmI == 0){
				if(key=='s')jugador1.velocidad.y=-4;
				if(key=='w')jugador1.velocidad.y=4;
			}else{
				if(key=='l')jugador2.velocidad.y=-4;
				if(key=='o')jugador2.velocidad.y=4;
			}
		}
	}
	pthread_exit(0);
}

void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
      return NULL; //Evita el warning
}

void* hilo_conexiones(void* d)
{
      CMundo* p=(CMundo*) d;
      p->GestionaConexiones();
      return NULL; //Evita el warning
}

void CMundo::GestionaConexiones(){
	//while ( puntos1 < VICTORY_SCORE && puntos2 < VICTORY_SCORE ) {
	while ( !acabar ) {
		Socket aux;
		char nombre_cliente[50];
		
		aux = socket_conexion.Accept();
		if( aux.Receive(nombre_cliente, sizeof(nombre_cliente)) < 0){
			perror("Error Servidor Recibiendo el Nombre del Cliente");
			//break;
		}
		else{
			printf("Nombre del cliente conectado: %s\n", nombre_cliente);
			aux.setNombre(nombre_cliente);
			if(!conexiones.empty()){ //Evita violación de acceso si el vector está vacío
				if(conexiones[0].getSock() == INVALID_SOCKET) //Comprueba si el jugador 1 está libre
					conexiones[0] = aux;
				else
					conexiones.push_back(aux);
			}
			else
				conexiones.push_back(aux);
			
			actualizaNombres();
		}
	}
	
	pthread_exit(0);
}

void CMundo::Init()
{	

	//MANEJO DE SEÑALES ////////////////
	struct sigaction acc;
	acc.sa_handler = captura;
	acc.sa_flags=0;
	sigemptyset(&acc.sa_mask);
	sigaction(SIGINT, &acc, NULL);
	sigaction(SIGTERM, &acc, NULL);
	sigaction(SIGPIPE, &acc, NULL);
	sigaction(SIGUSR2, &acc, NULL);

	// Abre el FIFO (LOGGER) ////////////////////
	fd_fifo = open(id_fifo, O_WRONLY);
	if ( fd_fifo < 0 ) {
		perror("No puede abrirse el FIFO");
	}
	
	// CONFIGURACIÓN SOCKETS
	char ip[] = "127.0.0.1";
	if(socket_conexion.InitServer(ip, 5000) < 0) //IP local y puerto 5000
		perror("Error Socket.InitServer()");
	
	/*	
	socket_comunicacion = socket_conexion.Accept();
	
	char nombre_cliente[50];
	if( socket_comunicacion.Receive(nombre_cliente, sizeof(nombre_cliente)) < 0)
		perror("Error Servidor Recibiendo el Nombre del Cliente: ");
	printf("Nombre del cliente conectado: %s\n", nombre_cliente);
	*/
	
	// INICIALIZA THREAD (Gestiona Conexiones)
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thid3, &attr, hilo_conexiones, this);
	pthread_attr_destroy(&attr);
	
	//printf("Nombre del Cliente Conectado: %s", socket_comunicacion.???);
	
	/*
	//FIFO SERVIDOR -> CLIENTE ///////////////////////////////
	// Abre el FIFO
	fd_fifo_servidor_a_cliente = open(id_fifo_servidor_a_cliente, O_WRONLY);
	if (fd_fifo_servidor_a_cliente < 0) {
		perror("No puede abrirse el FIFO: %s", id_fifo_servidor_a_cliente);
		exit(0);
	}
	
	//FIFO CLIENTE -> SERVIDOR ///////////////////////////////
	// Abre el FIFO
	fd_fifo_cliente_a_servidor = open(id_fifo_cliente_a_servidor, O_RDONLY);
	if (fd_fifo_cliente_a_servidor < 0) {
		perror("No puede abrirse el FIFO: %s", id_fifo_cliente_a_servidor);
		exit(0);
	}
	*/
	
	// INICIALIZA THREAD (RECIBE COMANDOS Jugador1)
	pthread_attr_t attr2;
	pthread_attr_init(&attr2);
	pthread_attr_setdetachstate(&attr2, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thid1, &attr2, hilo_comandos, this);
	pthread_attr_destroy(&attr2);
	
	// INICIALIZA THREAD (RECIBE COMANDOS Jugador2)
	pthread_attr_t attr3;
	pthread_attr_init(&attr3);
	pthread_attr_setdetachstate(&attr3, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thid2, &attr3, hilo_comandos, this);
	pthread_attr_destroy(&attr3);
	
	// INICIALIZACIONES MUNDO //////////////////////////////
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
}
