#include "DatosMemCompartida.h"
#include <assert.h>
#include <iostream>

//Open, fstat
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

//Perror
#include <stdio.h>

//Mmap
#include <sys/mman.h>

//Usleep
#include <unistd.h>

int main(int argc, char **argv) {

	if (argc!=1)  {
		fprintf (stderr, "%s no recibe parametros.\n", argv[0]);
		return(1);
	}
	
	int fd;
	DatosMemCompartida *c;
	struct stat bstat;
	
	fd=open("datosCom", O_RDWR);
	if ( fd < 0){
		perror("No puede abrirse el archivo");
		exit(1);
	}
	
	if(fstat(fd, &bstat) < 0){
		perror("fstat");
		exit(1);
	}
	
	c=static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	if(c == MAP_FAILED){
		perror("Error en mmap");
		exit(1);
	}
	close(fd);
	
	//printf("%ld, %ld\n", bstat.st_size, sizeof(DatosMemCompartida)); //Comprobación
	assert(sizeof(DatosMemCompartida) == bstat.st_size);
	
	float error = 0.0f;
	while(c->fin == 0){
		usleep(25000);
		error = c->esfera.centro.y - ( c->raqueta1.y1 + (c->raqueta1.y2 - c->raqueta1.y1) / 2.0f );
		if (error > 0)
			c->accion = 1;
		else	
			c->accion = -1;
	}
	
	munmap(c, sizeof(DatosMemCompartida));
	unlink("datosCom");

	return 0;
}
