// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include "DatosMemCompartida.h"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdio.h>
//#include <cstdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <math.h>
#include "comun.h"
#include <iostream>
#include <cassert>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	/*
	//Cierra los FIFO
	close(fd_fifo_servidor_a_cliente);
	unlink(id_fifo_servidor_a_cliente);
	
	close(fd_fifo_cliente_a_servidor);
	unlink(id_fifo_cliente_a_servidor);
	*/
	pDatosMemCom->fin = 1; //Indica al bot que termine
	usleep(10);
	
	unlink("datosCom");
	munmap(pDatosMemCom, sizeof(DatosMemCompartida));
	
	socket.Close();
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	

	char cad[200]; 
	/*
	//Lee la tubería cliente-servidor
	if(read(fd_fifo_servidor_a_cliente, cad, sizeof(cad)) < 0){
		perror("read FIFO");
	}
	*/
	
	//Recibe las coordenadas por el socket
	if( socket.Receive(cad, sizeof(cad)) < 0){
		perror("Servidor Desconectado");
		exit(0);
	}
	//Actualiza los datos
	sscanf(cad, "%f %f %f %f %f %f %f %f %f %f %f %d %d", 
		&esfera.centro.x, &esfera.centro.y, &esfera.radio, 
		&jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, 
		&jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, 
		&puntos1, &puntos2); 
	
	/*
	if(puntos1 >= 3 || puntos2 >= 3){
		pDatosMemCom->fin = 1; //Indica al bot que termine
		exit(1);
	}
	*/
		
	/*
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	//static mensaje m; //No es necesario inicializarlo continuamente
	
	//Colisión con las paredes
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	//Colisión de los jugadores con la pelota
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	
	if(fondo_izq.Rebota(esfera)) //Punto del jugador 2
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		//m = (mensaje){puntos2, "Jugador 2\0"};
		//write(fd_fifo, &m, sizeof(m));
		
		//Victoria
		if(puntos2 >= 3){
			printf("Jugador 2 Gana.\n");
			pDatosMemCom->fin = 1;
			exit(1);
		}
	}

	if(fondo_dcho.Rebota(esfera)) //Punto del jugador 1
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		//m = (mensaje){puntos1, "Jugador 1\0"};
		//write(fd_fifo, &m, sizeof(m));
		
		//Victoria
		if(puntos1 >= 3){
			printf("Jugador 1 Gana.\n");
			pDatosMemCom->fin = 1;
			exit(1);
		}
	}
	*/
	
	//Actualiza los datos de memoria compartida
	pDatosMemCom->esfera = esfera;
	pDatosMemCom->raqueta1 = jugador1;
	
	//Mueve al jugador controlado por la IA
	if(pDatosMemCom->accion == 1)
		OnKeyboardDown('w', 0, 0);
	else if (pDatosMemCom->accion == -1)
		OnKeyboardDown('s', 0, 0);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*
	switch(key){
		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;
	}
	*/
	
	//write(fd_fifo_cliente_a_servidor, &key, sizeof(key));
	
	if( socket.Send((char*)&key, sizeof(key)) < 0 ){
		perror("Error sending key");
		exit(-1);
	}
}

void CMundo::Init()
{	
	//CONFIGURACIÓN SOCKET
	char nombre_cliente[50];
	char ip[] = "127.0.0.1";
	printf("Introduzca el nombre del cliente: "); //Pide el nombre del cliente
	if(scanf("%[^\n]", nombre_cliente) == EOF) 
		perror("Error lectura del nombre del cliente");
		
	if( socket.Connect(ip, 5000) < 0 ) //0 asigna puerto disponible
		perror("Error en Cliente, Socket Connect");
	else
		printf("Conexión Realizada Exitosamente con el Servidor.\n");
	
	if( socket.Send(nombre_cliente, sizeof(nombre_cliente)) < 0) //Envía nombre del cliente
		perror("Error al Enviar el Nombre del Cliente");
	
	
	/*
	//FIFO SERVIDOR -> CLIENTE //////////////////////
	
	// Borra FIFO por si existía previamente
	unlink(id_fifo_servidor_a_cliente);
	
	// Crea el FIFO
	if (mkfifo(id_fifo_servidor_a_cliente, 0600) < 0) {
		perror("No puede crearse el FIFO");
		exit(0);
	}
	
	// Abre el FIFO
	fd_fifo_servidor_a_cliente = open(id_fifo_servidor_a_cliente, O_RDONLY);
	if (fd_fifo_servidor_a_cliente < 0) {
		perror("No puede abrirse el FIFO");
		exit(0);
	}
	
	//FIFO CLIENTE -> SERVIDOR //////////////////////
	
	// Borra FIFO por si existía previamente
	unlink(id_fifo_cliente_a_servidor);
	
	// Crea el FIFO
	if (mkfifo(id_fifo_cliente_a_servidor, 0600) < 0) {
		perror("No puede crearse el FIFO");
		exit(0);
	}
	
	// Abre el FIFO
	fd_fifo_cliente_a_servidor = open(id_fifo_cliente_a_servidor, O_WRONLY);
	if (fd_fifo_cliente_a_servidor < 0) {
		perror("No puede abrirse el FIFO");
		exit(0);
	}
	*/
	
	//INICIALIZACIONES DEL MUNDO ///////////////////////
	
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	
	// FICHERO COMPARTIDO EN MEMORIA /////////////////////
	
	int fd_memCom;
	struct stat bstat;
	DatosMemCompartida datosMemCom;
	
	fd_memCom=open("datosCom", O_RDWR | O_TRUNC | O_CREAT, 0666);
	if ( fd_memCom < 0){
		perror("No puede crearse el archivo (mundo.cpp : init : creat) ");
	}
		
	write(fd_memCom, &datosMemCom, sizeof(DatosMemCompartida));
	fstat(fd_memCom, &bstat);
	assert(sizeof(DatosMemCompartida) == bstat.st_size);
	
	//printf("%ld, %ld\n", bstat.st_size, sizeof(DatosMemCompartida)); //Comprobación
	
	pDatosMemCom = static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fd_memCom, 0));
	if (pDatosMemCom==MAP_FAILED){
		perror("Error en mmap");
		close(fd_memCom);
		exit(1);
	}
	close(fd_memCom);
	
}
