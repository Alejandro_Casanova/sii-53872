//Alumno: Alejandro Casanova Martín
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Socket.h"
#include <vector>

#define VICTORY_SCORE 5

class DatosMemCompartida;

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void RecibeComandosJugador();
	void OnTimer(int value);
	void OnDraw();	
	
	void nuevaPartida();
	void actualizaNombres();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	char nombreJugador1[50] = "?";
	char nombreJugador2[50] = "?";
	
	int acabar = 0;
	
	int fd_fifo = 0;
	char id_fifo[5] = "FIFO";
	
	/*
	int fd_fifo_servidor_a_cliente = 0;
	char id_fifo_servidor_a_cliente[22] = "FIFO_Servidor_Cliente";
	
	int fd_fifo_cliente_a_servidor = 0;
	char id_fifo_cliente_a_servidor[22] = "FIFO_Cliente_Servidor";
	*/
	
	pthread_t thid1 = 0; //Recibe Comandos Jugador1
	pthread_t thid2 = 0; //Recibe Comandos Jugador2
	pthread_t thid3 = 0; //Gestiona Conexiones
	
	Socket socket_conexion;
	//Socket socket_comunicacion;
	
	//Socket servidor;
   	std::vector<Socket> conexiones;
   	void GestionaConexiones();
   
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
