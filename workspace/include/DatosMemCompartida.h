#pragma once 
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida
{       
public:         
      Esfera esfera;
      Raqueta raqueta1;
      int accion = 0; //1 arriba, 0 nada, -1 abajo
      int fin = 0; //1 se acabó, 0 se sigue jugando
};
