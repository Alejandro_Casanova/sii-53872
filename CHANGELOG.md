# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1] - 2021-10-19
### Added
+ Git Ignore file
+ Readme file
+ Changelog File
+ Added my name at the top of the header files.

## [2.0] - 2021-11-02
### Added
+ Implemented movement in Raqueta.cpp and Esfera.cpp
+ Ball slowly shrinks as the game progresses and stops when size is 1/5. Implemented in Esfera.cpp

## [3.0] - 2021-12-06
### Added
+ Implemented logger to keep track of scoring
+ Implemented bot that plays for player1
+ Match finishes when either player scores 3 points

## [4.0] - 2021-12-16
### Changed
+ Separated "tenis" into "servidor" & "cliente"
### Added
+ Communication between "servidor" & "cliente" (coordinates and keys pressed)
+ Thread in "servidor" to handle key presses sent from "cliente"
+ Signal handling in "servidor"
### Fixed
+ "Bot" again exits when game finishes

## [5.0] - 2021-12-16
### Changed
+ Communication between "servidor" and "cliente" is now performed with sockets

## [5.1] - 2022-01-08
### Changed
+ Simultaneous conexion of several clients
+ Server shut-down
+ Management of client disconexion
+ Game restarts after scoring 5 points

## [5.2] - 2022-01-08
### Added
+ Display player names on server screen

