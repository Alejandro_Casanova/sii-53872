# Repositorio de Prácticas de SII - Curso 2021-2022

## Autor: Alejandro Casanova

### Instrucciones:
Jugador 1: 'w' - Subir Raqueta, 's' - Bajar Raqueta

Jugador 2: 'o' - Subir Raqueta, 'l' - Bajar Raqueta

Gana el Jugador que marque 5 puntos primero.

Orden de ejecución: logger -> servidor -> cliente -> bot
